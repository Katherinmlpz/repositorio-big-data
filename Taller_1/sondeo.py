import sys
import db
from modelo import *

# python3 sondeo.py inacutacion.csv 10
DEPARTAMENTOS = []
MUNICIPIOS = []


def add(modelo):
    db.session.add(modelo)
    db.session.commit()

def buscar_dept(departamento):
    dept = db.session.query(Departamento).where(Departamento.departamento==departamento).first()
    return dept.id

def buscar_muni(municipio):
    muni = db.session.query(Municipio).where(Municipio.municipio==municipio).first()
    return muni.codigodane

def modelo_ina(codigodane, clasebien, fecha, cantidad):
    inca = Incautacion(
        clasebien=clasebien,
        fecha=fecha,
        cantidad=cantidad,
        id_muni=codigodane
    )
    return inca

def modelo_dpt(departamento):
    dept = Departamento(
        departamento=departamento
    )
    return dept

def modelo_muni(codigodane, municipio, id_dept):
    muni = Municipio(
        codigodane=codigodane,
        municipio=municipio,
        id_dept=id_dept
    )
    return muni

if __name__ == "__main__":

    db.Base.metadata.create_all(db.motor)
    arg = sys.argv
    if len(arg) == 3:
        doc = arg[1]
        reg = arg[2]
        data_length = int(reg)
        file = open(doc, mode="r")
        br = 0
        ignorar = True
        for line in file:
            line = line.strip()
            row = line.split(",")
            if ignorar:
                ignorar = False
            else:
                departamento = row[0]
                municipio = row[1]
                codigodane = row[2]
                clasebien = row[3]
                fecha = row[4]
                cantidad = float(row[5].replace(".",""))

                if not departamento in DEPARTAMENTOS:
                    DEPARTAMENTOS.append(departamento)
                    dept_model = modelo_dpt(departamento)
                    add(dept_model)


                if not municipio in MUNICIPIOS:
                    MUNICIPIOS.append(municipio)
                    id_dept = buscar_dept(departamento)
                    if id_dept:
                        muni_model = modelo_muni(codigodane, municipio, id_dept)
                        add(muni_model)
                    else:
                        print(f"No se encuentra el departamento {departamento}")


                id_muni = buscar_muni(municipio)
                if id_muni:
                    inca_model = modelo_ina(id_muni, clasebien, fecha, cantidad)
                    add(inca_model)
                else:
                    print(f"No se encuentra el municipio {municipio}")

            if br >= data_length:
                print("Se insertaron de forma exitosa")
                break
            br += 1
