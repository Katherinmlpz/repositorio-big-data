from sqlalchemy.sql.schema import ForeignKey
import db
from sqlalchemy import Column, Integer, Text, String, ForeignKey, Float

#['DEPARTAMENTO','MUNICIPIO','CODIGO DANE','CLASE BIEN','FECHA HECHO','CANTIDAD']

class Departamento(db.Base):
    __tablename__ = "departamento"
    id = Column(Integer, primary_key=True)
    departamento = Column("departamento", Text)
    
class Municipio(db.Base):
    __tablename__ = "municipio"
    codigodane = Column("codigodane", String(10), primary_key=True)
    municipio = Column("municipio", Text)
    id_dept = Column(Integer, ForeignKey("departamento.id"))

class Incautacion(db.Base):
    __tablename__ = "incautacion"
    id = Column(Integer, primary_key=True)
    clasebien = Column("clasebien", Text)
    fecha = Column("fecha", Text)
    cantidad = Column("cantidad", Float)
    id_muni = Column(String(10), ForeignKey("municipio.codigodane"))