import luigi
#python3 ejemploluigi.py Cuadrados --local-scheduler
#python3 ejemploluigi.py Cuadrados --local-scheduler --n 30
class SalidaNumeros(luigi.Task):
    n=luigi.IntParameter()
    def requires(self):
        return[]
    def output(self):
        return luigi.LocalTarget("numeros_{}.txt".format(self.n))
        ''' return luigi.LocalTarget("numeros.txt")'''
    def run(self):
        
        with self.output().open('w') as f:
            for i in range(1, self.n):
                f.write('{}\n'.format(i))
                
class Cuadrados(luigi.Task):
    n=luigi.IntParameter(default=10)
    def requires(self):
        return [SalidaNumeros(n=self.n)]
    def output(self):
         return luigi.LocalTarget("cuadrados_{}.txt".format(self.n))
    def run(self):
        with self.input()[0].open() as fin, self.output().open('w') as fout:
            for linea in fin:
                n= int(linea.strip())
                val = n**2
                fout.write('{}:{}\n'.format(n, val))
                
if __name__=='__main__':
    luigi.run()
   