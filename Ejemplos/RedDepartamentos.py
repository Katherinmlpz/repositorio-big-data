import sys

salida={}

#Agrupacion
for linea in sys.stdin:
    linea=linea.strip()
    dpt,n = linea.split('\t')
    if dpt  in salida:
        salida[dpt].append(int(n))
    else:
        salida[dpt]=[]
        salida[dpt].append(int(n))


#Reduccion
for dpt in salida.keys():
    ls_n=salida[dpt]
    sum_g=0
    for g in ls_n:
        sum_g+=g
    pr_n = sum_g/len(salida[dpt])
    print(dpt,'promedio n: ', pr_n)
     

