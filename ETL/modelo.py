from sqlalchemy.sql.schema import ForeignKey
import db
from sqlalchemy import Column, Integer, Text, Boolean, String, ForeignKey, Float
#['DEPARTAMENTO','MUNICIPIO','CODIGO DANE','CLASE BIEN','FECHA HECHO','CANTIDAD']
class Incautacion(db.Base):
    __tablename__='incautacion'
    id=Column(Integer, primary_key = True )
    departamento=Column('departamento', Text)
    municipio = Column('munincipio', Text)
    codigoDane=Column('codigoDane', Text)
    claseBien=Column('claseBien', Text)
    fechaHecho=Column('fechaHecho', Text)
    cantidad = Column('cantidad', Float)
    
    
    
    class Comentario(db.Base):
        __tablename__='comentario'
        id=Column(Integer, primary_key = True)
        mensaje=Column('mensaje', String(20))
        id_u=Column(Integer,ForeignKey('usuario.id'))